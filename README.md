
# TIS-100 SOLUTIONS

These are my solutions for the game TIS-100 by zachtronics. As of 88324d1, everything is my own work. (and all problems have been solved).

PR's are welcome :smile:, there is still a lot of performance improvments that could be made...

Simply drop a soltuion into your save folder `~/.local/share/TIS-100/*/save` in order to try them out.

# License

GPLv3. Feel free to use these yourself (but don't spoil your game)!
